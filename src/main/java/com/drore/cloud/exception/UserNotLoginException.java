package com.drore.cloud.exception;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/06/20 11:50
 */

public class UserNotLoginException extends RuntimeException {
    public UserNotLoginException() {
        // TODO Auto-generated constructor stub
    }
    public UserNotLoginException(String msg){
        super(msg);
    }
}
