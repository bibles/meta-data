package com.drore.cloud.controller;

import com.drore.cloud.domain.sys.RestMessage;
import com.drore.cloud.exception.ApiException;
import com.drore.cloud.sdkjdbc.core.JdbcRunner;
import com.drore.cloud.utils.DSUtil;
import com.drore.cloud.utils.GsonUtil;
import com.drore.cloud.utils.MapClearUtil;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明  开发者操作
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/07/10 15:05
 */
@Api("开发者模块")
@RequestMapping(value = "/dbm/developer")
@RestController
public class developerCtl {
    @Autowired
    private JdbcRunner run;

    @ApiOperation("新增开发者")
    @RequestMapping(value = "/save" ,method = {RequestMethod.POST, RequestMethod.GET})
    public RestMessage save(String resource_name, String submit_data)  {
        //开发者保存流程  每个用户对应一个数据库
        RestMessage rm=new RestMessage();
        Gson gson = GsonUtil.create();
        Map<String, Object> map = (Map<String,Object>)gson.fromJson(submit_data, Map.class);
        if(map.get("file")!=null){
            map.remove("file");
        }
        String insert=null;
        map= MapClearUtil.clear(map);
        //关联数据库类型
        String data_source_id = Objects.toString(map.get("data_source_id"));

        Map<String, Object> data_source_info = run.queryOne("data_source_info", data_source_id);

        Map<String, Object> dataConfigInfo = run.queryOne("db_instance_info", Objects.toString(data_source_info.get("instance_id")));
        String data_type = Objects.toString(dataConfigInfo.get("data_type"));
        map.put("data_type",data_type);
        map.put("status","wait_issue");
        if(map.get("id")==null){
            insert=run.insert(resource_name,map);
        }else {
            insert=run.update(resource_name,map);
        }
        rm.setData(insert);
        return rm;
    }

    @ApiOperation("开发者发布")
    @RequestMapping(value = "/issue" ,method = {RequestMethod.POST, RequestMethod.GET})
    public RestMessage issue(String id)  {
        RestMessage rm=new RestMessage();
        //开发者信息
        Map<String, Object> developer_info = run.queryOne("developer_info", id);
        //数据库信息
        Map<String, Object> data_source_info = run.queryOne("data_source_info", Objects.toString(developer_info.get("data_source_id")));
        String instance_id = Objects.toString(data_source_info.get("instance_id"));
        //数据实例信息
        Map<String, Object> db_instance_info = run.queryOne("db_instance_info", instance_id);
        //配置数据源
        BasicDataSource ds = DSUtil.newBDS(db_instance_info);
        JdbcRunner jdbc = new JdbcRunner();
        jdbc.setDataSource(ds);
        //3将 获得的信息  组装成  用户授权语句
        String appid = Objects.toString(developer_info.get("appid"));
        String appscrect = Objects.toString(developer_info.get("appscrect"));
        StringBuffer grant_sql = new StringBuffer(" grant all PRIVILEGES on ");
        grant_sql.append(data_source_info.get("prefix_name"));
        grant_sql.append(".* to '").append(appid).append("'@'%'").append("IDENTIFIED by '").append(appscrect).append("' ;");
        boolean b;
        try {
            b = jdbc.executSql(grant_sql.toString());
            //添加到系统中 立即可以使用
            jdbc.getDataSource().close();

        } catch (SQLException e) {
            throw new ApiException("开发者发布失败:"+e.getLocalizedMessage());
        }
        rm.setSuccess(b);
        rm.setData("开发者发布成功");
        //创建成功后 修改开发者的发布状态
        run.update("developer_info", ImmutableMap.of("id",id,"status","issued"));
//        //创建成功后 修改资源的发布状态
//        run.update("data_source_info", ImmutableMap.of("id",id,"status","issued"));
//
//        db_instance_info.put("url",Objects.toString(db_instance_info.get("url"))+"/"+Objects.toString(data_source_info.get("prefix_name")));
//        //将新建的数据库 配置到系统中
//        BasicDataSource dsnew = DSUtil.newBDS(db_instance_info);
//        JdbcRunner jdbcnew = new JdbcRunner();
//        jdbcnew.setDataSource(dsnew);
//        mult.addDS(Objects.toString(data_source_info.get("id")),jdbcnew);
        return rm;
    }
}
