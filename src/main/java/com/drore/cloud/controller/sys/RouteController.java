package com.drore.cloud.controller.sys;

import com.drore.cloud.constant.LocalConstant;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a>
 * 2017/08/18 15:23
 */
@Api("未登录重定向拦截")
@Controller
public class RouteController {
    @RequestMapping(value = {"/"})
    public String index(HttpServletRequest request) throws IOException {
        HttpSession session=request.getSession();
        if (session.getAttribute(LocalConstant.SESSION_CURRENT_USER)==null) {
            return "forward:/login.html";
        }else {
            return "forward:/index.html";
        }
    }
}
