package com.drore.cloud.controller;

import com.drore.cloud.domain.sys.RestMessage;
import com.drore.cloud.sdkjdbc.core.JdbcRunner;
import com.drore.cloud.sdkjdbc.model.Pagination;
import com.drore.cloud.sdkjdbc.model.QueryParam;
import com.drore.cloud.utils.GsonUtil;
import com.drore.cloud.utils.MapClearUtil;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/***
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2017/09/19 13:48
 */
@Api("通用接口(前端看这里)")
@RestController
public class LayuiCmsCtl {

    @Autowired
    private JdbcRunner run;

    @RequestMapping(value = "/layui/table/save" ,method = {RequestMethod.POST, RequestMethod.GET})
    public RestMessage save(String resource_name, String submit_data) {
        RestMessage rm=new RestMessage();
        Gson gson = GsonUtil.create();
        Map<String, Object> map = (Map<String,Object>)gson.fromJson(submit_data, Map.class);
        if(map.get("file")!=null){
            map.remove("file");
        }
        String insert=null;
        map=MapClearUtil.clear(map);
        if(map.get("id")==null){
             insert=run.insert(resource_name,map);
        }else {
             insert=run.update(resource_name,map);
        }
        rm.setData(insert);
        return rm;
    }

    @ApiOperation("通用删除接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "resource_name",value = "表名"),
            @ApiImplicitParam(name = "id",value = "id")
    })

    @RequestMapping(value = "/layui/table/delete",method = {RequestMethod.GET, RequestMethod.POST})
    public RestMessage delete(@RequestParam(value = "id")String id,@RequestParam(value = "resource_name")String resource_name){
        RestMessage rm=new RestMessage();
        //判断是否有id
        if (StringUtils.isNotBlank(id)){
            //id存在,则更加id删除
            String pkid = run.delete(resource_name, id);
            rm.setId(pkid);
            rm.setMessage("删除成功");
        }
        return  rm;
    }

    @ApiOperation("通用列表接口(已经支持复杂查询)")
    @PostMapping("/layui/table/list")
    public Pagination<Map> list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10")Integer limit, String querydata, String resource_name ) {

        System.out.println("前端查询对象json:"+querydata);

        QueryParam query=new QueryParam(page,limit);
        query.addSort("create_time","desc");

        QueryParam.Param param = GsonUtil.create().fromJson(querydata, QueryParam.Param.class);
        query.setParam(param);
        Pagination pagination=new Pagination();
        if(StringUtils.isNotBlank(querydata)){
            pagination = run.queryListByExample(resource_name, query);
        }
        return pagination;
    }

    @ApiOperation("通用详情接口1(用id定位)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "resource_name",value = "表名"),
            @ApiImplicitParam(name = "id",value = "id")
    })
    @GetMapping("/layui/table/detail")
    public RestMessage detail(String id,String resource_name) {
        RestMessage rm = new RestMessage();
        if(StringUtils.isNotBlank(id)){
            Map<String, java.lang.Object> sys_user_info = run.queryOne(resource_name, id);
            rm.setData(sys_user_info);
        }
        return rm;
    }

}
