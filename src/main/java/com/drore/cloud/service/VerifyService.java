package com.drore.cloud.service;


import com.drore.cloud.domain.sys.ImageVerify;

import javax.servlet.http.HttpSession;

public interface VerifyService {

	ImageVerify generate(HttpSession session);
	ImageVerify process(HttpSession session);

//	boolean verify(String code, int x, int y);
	


}
