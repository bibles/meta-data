package com.drore.cloud.domain;

import java.math.BigDecimal;

public class Reimbursement {
    private BigDecimal personal_balance;//个人余额
    private BigDecimal fund_cost;//基金费用
    private BigDecimal fund_subsidy;//基金补助
    private BigDecimal personal_expense;//个人支出
    private BigDecimal personal_account_cost;//个账费用
    private BigDecimal personal_account_expense;//个账支出
    private BigDecimal total_reimbursement;//报销总额
    private String cost_type;//费用类别
    private BigDecimal total_cost;//总费用
    private String create_time;//创建时间
    private String operator;//操作员姓名
    private String operator_code;//操作员编号
    private String username;//员工姓名
    private String usercode;//员工工号
    private String id;//主键
    private String image_set;//图集
    private String finance_confirm;//财务确认标识 true和false

    private Integer birthyear;//出生年份

    public Integer getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(Integer birthyear) {
        this.birthyear = birthyear;
    }

    public String getFinance_confirm() {
        return finance_confirm;
    }

    public void setFinance_confirm(String finance_confirm) {
        this.finance_confirm = finance_confirm;
    }

    public String getImage_set() {
        return image_set;
    }

    public void setImage_set(String image_set) {
        this.image_set = image_set;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getPersonal_balance() {
        return personal_balance;
    }

    public void setPersonal_balance(BigDecimal personal_balance) {
        this.personal_balance = personal_balance;
    }

    public BigDecimal getFund_cost() {
        return fund_cost;
    }

    public void setFund_cost(BigDecimal fund_cost) {
        this.fund_cost = fund_cost;
    }

    public BigDecimal getFund_subsidy() {
        return fund_subsidy;
    }

    public void setFund_subsidy(BigDecimal fund_subsidy) {
        this.fund_subsidy = fund_subsidy;
    }

    public BigDecimal getPersonal_expense() {
        return personal_expense;
    }

    public void setPersonal_expense(BigDecimal personal_expense) {
        this.personal_expense = personal_expense;
    }

    public BigDecimal getPersonal_account_cost() {
        return personal_account_cost;
    }

    public void setPersonal_account_cost(BigDecimal personal_account_cost) {
        this.personal_account_cost = personal_account_cost;
    }

    public BigDecimal getPersonal_account_expense() {
        return personal_account_expense;
    }

    public void setPersonal_account_expense(BigDecimal personal_account_expense) {
        this.personal_account_expense = personal_account_expense;
    }

    public BigDecimal getTotal_reimbursement() {
        return total_reimbursement;
    }

    public void setTotal_reimbursement(BigDecimal total_reimbursement) {
        this.total_reimbursement = total_reimbursement;
    }

    public String getCost_type() {
        return cost_type;
    }

    public void setCost_type(String cost_type) {
        this.cost_type = cost_type;
    }

    public BigDecimal getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(BigDecimal total_cost) {
        this.total_cost = total_cost;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator_code() {
        return operator_code;
    }

    public void setOperator_code(String operator_code) {
        this.operator_code = operator_code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }
}
