package com.drore.cloud.domain.sys;

import java.io.Serializable;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 返回消息基础封装类
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/06/20 12:01
 */

public class RestMessage implements Serializable {

    private boolean success=true;
    private Object data;
    private Integer errCode;
    /***
     * 新增、修改主鍵返回id
     */
    private String id;

    //成功,错误返回提示信息
    private String message;


    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RestMessage() {
        super();
    }

    public RestMessage(Object data) {
        super();
        this.data = data;
    }

    public RestMessage(boolean success, Object data) {
        super();
        this.success = success;
        this.data = data;
    }


}
