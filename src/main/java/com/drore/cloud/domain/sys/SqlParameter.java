/*
 * Copyright (C) 2018 v5link Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.v5link.com.
 * Developer Web Site: http://open.v5link.com.
 */

package com.drore.cloud.domain.sys;


/***
 * SQL参数
 * @since:province_cdc_III 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2018/04/06 15:54
 */
public class SqlParameter {

    /***
     * 字段类型
     */
    private String columnField;

    /***
     * 参数值
     */
    private Object paramValue;

    public  SqlParameter(String columnField,Object paramValue){
        this.columnField=columnField;
        this.paramValue=paramValue;
    }


    public String getColumnField() {
        return columnField;
    }

    public void setColumnField(String columnField) {
        this.columnField = columnField;
    }


    public Object getParamValue() {
        return paramValue;
    }

    public void setParamValue(Object paramValue) {
        this.paramValue = paramValue;
    }
}
