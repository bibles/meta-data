package com.drore.cloud.domain.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * 图形验证码
 */
public class ImageVerify implements Serializable{
	
	private static final long serialVersionUID = -8529638529631L;

	private Date inValid;					// 图形验证码有效时间
	
	private int x;							// 拖拉的X
	
	private int y;							// 拖拉的Y
	
	private int offset;						// 最大偏移量
	
	private String verifyCode;				// 验证码Code
	
	private String original;				// 原图
	
	private String whole;					// 破图
	
	private String repair;					// 补图
	
	private String point;					// 破图坐标点
	
	private int repairWidth;				// 补图的宽度
	
	private int repairHeight;			// 补图的高度
	
	public Date getInValid() {
		return inValid;
	}

	public void setInValid(Date inValid) {
		this.inValid = inValid;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getWhole() {
		return whole;
	}

	public void setWhole(String whole) {
		this.whole = whole;
	}

	public String getRepair() {
		return repair;
	}

	public void setRepair(String repair) {
		this.repair = repair;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public int getRepairWidth() {
		return repairWidth;
	}

	public void setRepairWidth(int repairWidth) {
		this.repairWidth = repairWidth;
	}

	public int getRepairHeight() {
		return repairHeight;
	}

	public void setRepairHeight(int repairHeight) {
		this.repairHeight = repairHeight;
	}

}
