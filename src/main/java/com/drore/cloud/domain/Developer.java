package com.drore.cloud.domain;

import com.drore.cloud.domain.sys.SystemModel;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/07/17 17:32
 */

public class Developer extends SystemModel {
    /**开发者账号
     * */
    private String appid;
    /**开发者秘钥
     * */
    private String appscrect;
    /**开发者名称
     * */
    private String username;
    /**开发者角色
     * */
    private String role;
    /**对应数据库类型
     * */
    private String data_type;
    /**数据源id
     * */
    private String data_source_id;
    /**开发者状态
     * */
    private String status;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppscrect() {
        return appscrect;
    }

    public void setAppscrect(String appscrect) {
        this.appscrect = appscrect;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getData_source_id() {
        return data_source_id;
    }

    public void setData_source_id(String data_source_id) {
        this.data_source_id = data_source_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
