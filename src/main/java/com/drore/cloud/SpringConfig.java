package com.drore.cloud;

import com.drore.cloud.jdbc.MultiDS;
import com.drore.cloud.sdkjdbc.core.JdbcRunner;
import com.drore.cloud.sdkjdbc.model.Pagination;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Configuration
public class SpringConfig {
    @Value(value = "${cms.database.driver-class-name}")
    private String DriverClassName;
    @Value(value = "${cms.database.url}")
    private String url;
    @Value(value = "${cms.database.username}")
    private String username;
    @Value(value = "${cms.database.password}")
    private String password;

    @Bean(autowire = Autowire.BY_NAME,value = "run")
    public JdbcRunner InitResource() throws SQLException {
        JdbcRunner runner = new JdbcRunner();
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(DriverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaxActive(20);
        dataSource.setValidationQuery("select  1");
        dataSource.setMaxWait(6000);
        dataSource.setTimeBetweenEvictionRunsMillis(3);
        runner.setDataSource(dataSource);
        return runner;
    }

    @Bean(autowire = Autowire.BY_NAME,value = "templateMap")
    public MultiDS TemplateMap() throws SQLException {

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaxActive(20);
        dataSource.setMaxWait(6000);

        JdbcRunner run_one = new JdbcRunner();
        run_one.setDataSource(dataSource);

        MultiDS tpmap= new MultiDS();
        //获取所有已发布数据源信息
        Pagination data_source_page = run_one.queryListByExample("data_source_info", ImmutableMap.of("status", "issued"), 1, 100);
        //获取所有数据库实例信息
        Pagination<Map<String,Object>> configs = run_one.queryListByExample("db_instance_info");

        //组装成可以使用的数据库连接信息
        if(data_source_page.getCount()>0&&configs.getCount()>0){
            List<Map<String,Object>> sources = data_source_page.getData();
            for (Map<String, Object> source : sources) {
                String instance_id = Objects.toString(source.get("instance_id"));
                for (Map<String, Object> instance : configs.getData()) {
                    String id = Objects.toString(instance.get("id"));
                    if(instance_id.equals(id)){
                        //数据源与数据库实例匹配到
                        BasicDataSource ds = new BasicDataSource();
                        ds.setDriverClassName(Objects.toString(instance.get("driver_class_name")));
                        ds.setUrl(Objects.toString(instance.get("url"))+"/"+Objects.toString(source.get("prefix_name")));
                        if (instance.get("username")!=null){
                            ds.setUsername(Objects.toString(instance.get("username")));
                        }
                        if (instance.get("password")!=null){
                            ds.setPassword(Objects.toString(instance.get("password")));
                        }
                        ds.setMaxActive(20);
                        ds.setMaxWait(6000);

                        JdbcRunner jdbc = new JdbcRunner();
                        jdbc.setDataSource(ds);
                        tpmap.addDS(Objects.toString(source.get("id")),jdbc);
                    }
                }
            }
        }
        return tpmap;
    }
}