package com.drore.cloud.utils;

import com.google.gson.Gson;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/06/20 12:26
 */

public class ThreadLocalHolder {

    private static final ThreadLocal<HttpSession> th_session=new ThreadLocal<HttpSession>();
    private static Map sessionMap=new HashMap();
    private static final ThreadLocal<String> th_sessionid = new ThreadLocal();

    public static String getSessionId() {
        return (String)th_sessionid.get();
    }
    public static void setSessionId(String SessionId) {
        th_sessionid.set(SessionId);
    }

    public static HttpSession getSession(){
        return th_session.get();
    }

    public static void setSession(HttpSession session){
        th_session.set(session);
    }
    public static void setSessionMap(String SessionId,Object o ){
        sessionMap.put(SessionId,o);
    }
    public static Map getSessionMap(){
        return sessionMap;
    }

    public static Object getCurrentUser(){
        return sessionMap.get(getSessionId());
    }
    public static void removeCurrentUser(){
        sessionMap.remove(getSessionId());
    }
}
