package com.drore.cloud.utils;

import org.apache.commons.dbcp.BasicDataSource;

import java.util.Map;
import java.util.Objects;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/07/16 19:27
 */

public class DSUtil {
    public static BasicDataSource newBDS(Map config){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(Objects.toString(config.get("driver_class_name")));
        ds.setUrl(Objects.toString(config.get("url")));
        if(config.get("username")!=null){
            ds.setUsername(Objects.toString(config.get("username")));
        }
        if(config.get("password")!=null){
            ds.setPassword(Objects.toString(config.get("password")));
        }
        return ds;
    }
}
