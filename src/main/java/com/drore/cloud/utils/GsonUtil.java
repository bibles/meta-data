package com.drore.cloud.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/06/20 12:07
 */

public class GsonUtil {
    public static Gson create(){
        return new Gson();
    }

    public static Gson createByDateFormat(String pattern){
        GsonBuilder gb=new GsonBuilder();
        gb.setDateFormat(pattern);

        return gb.create();
    }
    public static Gson createByDateFormat(){
        GsonBuilder gb=new GsonBuilder();
        gb.setDateFormat("yyyy-MM-dd HH:mm:ss");
        return gb.create();
    }
    /***
     * 对json非空判断，获取string值
     * @param json
     * @return
     */
    public static String toStringValue(JsonElement json){
        if (json!=null&&!json.isJsonNull()){
            return json.getAsString();
        }
        return "";
    }
}
