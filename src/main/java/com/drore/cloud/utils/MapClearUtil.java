package com.drore.cloud.utils;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Objects;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/05/02 18:50
 */

public class MapClearUtil {

    public static Map<String,Object> clear(Map<String,Object> map){
        Map<String,Object> newm=Maps.newHashMap();
        for (Map.Entry<String,Object> m: map.entrySet()){
            if(m.getValue()!=null){
                if(!Objects.toString(m.getValue()).equals("")){
                    newm.put(m.getKey(),m.getValue());
                }
            }
        }
        return newm;
    }


}
