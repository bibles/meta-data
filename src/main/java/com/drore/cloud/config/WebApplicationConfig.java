/*
 * Copyright (C) 2016 Zhejiang DRORE Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.drore.com.
 * Developer Web Site: http://open.drore.com.
 */

package com.drore.cloud.config;


import com.drore.cloud.filter.GlobalRequestMappingFilter;
import com.drore.cloud.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.Filter;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a>
 * 2017/09/18 10:24
 */
@Configuration
public class WebApplicationConfig extends WebMvcConfigurerAdapter{

    /***
     * 添加拦截器
     * @param
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //登录判断
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(new String[]{"/cms/slide_verify/**","/cms/user/login","/v2/api-docs","/doc.html"});
        super.addInterceptors(registry);
    }

    /**
     * encoding编码问题
     * @return
     */
    @Bean
    public Filter characterEncodingFilter(){
        CharacterEncodingFilter characterEncodingFilter=new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

    @Bean
    public Filter globalRequestMappingFilter(){
        GlobalRequestMappingFilter globalRequestMappingFilter=new GlobalRequestMappingFilter();
        return globalRequestMappingFilter;
    }
}
