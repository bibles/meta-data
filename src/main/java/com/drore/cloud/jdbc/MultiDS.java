package com.drore.cloud.jdbc;

import com.drore.cloud.sdkjdbc.core.JdbcRunner;

import java.util.HashMap;
import java.util.Map;

/***
 * 浙江卓锐科技股份有限公司 版权所有@Copyright 2016
 * 说明
 * 项目名称
 * @since:cloud-ims 1.0
 * @author <a href="mailto:baoec@drore.com">baoec@drore.com </a> 
 * 2018/07/09 15:45
 */

public class MultiDS {
    private Map<String,JdbcRunner> switchDSPools=new HashMap<>();

    public void addDS(String dsId,JdbcRunner runner){
        this.switchDSPools.put(dsId,runner);
    }

    public JdbcRunner getSessionRunner(String dsId){
        return this.switchDSPools.get(dsId);
    }
}
