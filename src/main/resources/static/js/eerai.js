var eerai = (function() {
	var canvas,code,whole,repair,point,that,createUrl,currentY,width,height,repairW,repairH,image;
	var eerai_ = function(createUrl){
		this.canvas = "cover_content";
		canvas="cover_content";
		that = this;
		$.ajax({
			url : createUrl,
			type : 'post',
		    dataType:"json", 
			async: false,//使用同步的方式,true为异步方式
			success : function(data){
				if(data.success){
					var data=data.data;

					that.whole = "/cms/slide_verify/load?code=" + data["whole"]+'&'+new Date().getTime();
					that.repair = "/cms/slide_verify/load?code=" + data["repair"]+'&'+new Date().getTime();
					that.original = "/cms/slide_verify/load?code=" + data["original"]+'&'+new Date().getTime();
					that.point = $.parseJSON(data["point"]);
					that.repairW = data["repairWidth"];
					that.repairH = data["repairHeight"];
					that.currentY = data["y"];
					//that.x = data["x"];
					//that.y = data["y"];
					that.code = data["verifyCode"];
				}else {
					//console.info(data);
					layer.msg(data.message)
					$(".dragdealer").hide()

					$("#chongshi").css("display","inline-block")
					return false;
				}

			}
		});
		var re=new Image();
		re.src=that.repair;
		that.image = new Image();
		that.image.src = that.whole;
		//image.src=that.original;
		// $("#cover_content").mouseover(function () {
		// 	$(".allqq").show()
		// })
		$(".red-bar").mousedown(function(){
			$(".tips-text").hide()
		});

		// $(".red-bar").mouseup(function(){
		// 	$(".tips-text").show()
        //
		// });
		$("#cover_content").mouseleave(function () {
			$(".allqq").css("display",'none');
		});
		// $("#cover_content").mouseover(function () {
		// 	$(".allqq").show()
		// })
		$(".red-bar").mousedown(function(){
			$(".tips-text").hide()
		});

		// $(".red-bar").mouseup(function(){
		// 	$(".tips-text").show()
        //
		// });
		$("#cover_content").mouseleave(function () {
			$(".allqq").hide()
		})
		
		//console.info("test");

		that.image.onload = function(){
			that.width = that.image.width;
			that.height = that.image.height;
			var html = '<div class="allqq" style="display: none"><canvas width="'+that.width +'" height="'+that.height +'" id="canvas_whole" style="display: block;">建议升级浏览器</canvas>';
			html += '<div id="conver_repair" style="display:block;position:absolute;width: '+that.repairW +';height:'+that.repairH+';'+
					'background-image:url(\''+re.src+'\');top:'+that.currentY+'"></div></div>';
			html += '<div style="padding-top: 10px;">';
			html += '<div id="slider" class="dragdealer" style="border-radius: 20px;">';
			html += '<div class="handle red-bar" style="border-radius: 20px;"><span style="display: inline-block;border-radius: 20px;">|||</span></div>';
			html += '<span class="tips-text" style="display: inline-block;float: right;margin-right: 15px;line-height: 30px;color: #8c8c8c;">向右滑动滑块填充拼图</span>';
			html += '</div>';
			html += '</div>';

			$("#"+canvas).html(html);
			$("#"+canvas).css("width",that.image.width).css("height",that.image.height+100);


			// console.info(image.width);
			// console.info(image.height);
			
			
			canvas = document.getElementById("canvas_whole");  
		    context = canvas.getContext("2d");  
		    context.clearRect(0, 0, canvas.width, canvas.height)  

			context.drawImage(that.image,0,0);
			//$(canvas).css("display","block");
			
			var b=true;
			 new Dragdealer('slider', {
		    	  animationCallback: function(x, y) {
		    		if(x > 0){
		   				var value = new BigDecimal(x+"").multiply(new BigDecimal(that.width + "")).subtract(new BigDecimal(x+"").multiply(new BigDecimal(that.repairW+"")));	
			   			//console.info(value.toString());
			   			if(value.compareTo(BigDecimal.ZERO) > 0){
				    	    var $conver = $("#conver_repair");
							if(value>1){
								$(".tips-text").hide(1000);
							}
							if(b){
								$(".allqq").show();
							}

							//$(".allqq").show()
				    		//console.info(value.toString());
			    			$conver.css("left",value.toString() +"px");	
			   			}
		    		}

		    	  },callback:function(x,y){
		    		  var value = new BigDecimal(x+"").multiply(new BigDecimal(that.width + "")).subtract(new BigDecimal(x+"").multiply(new BigDecimal(that.repairW+"")));
		    		  var data = {"code":that.code,"value":parseInt(value.toString())};
					 verify(data)
					 b=false;
		    	  }
		   	});
		}
		
	}


    return eerai_;
}());

/**	
 * 	图片上拖拉
 
  //	绘制图片坐标
         var wholeX= $("#cover_content").offset().left;
         var wholeY= $("#cover_content").offset().top;
         //设置画布宽高
         var canvasW = 400;
         var canvasH = 200;
         var minWidth  = 0;
         var minHeight = 0;
         var maxWidth  = canvasW - 50;
         var maxHeight = canvasH - 50;
         console.info(" wholeX  :"+wholeX  +"|  wholeY :"+wholeY);
         //区别moueseup与click的标志
         var moveFlag=false;
         var clickFlag=false;
         var $conver = $("#conver_repair"); 
         $conver.css("background-image","url("+repair+")");
         //	拖拽函数
         $conver.mouseup(function(){
        	 moveFlag=false;
        	 var currentX = $(this).position().left;
        	 var currentY = $(this).position().top;
             console.info("currentX : " +currentX +"__  currentY:"+currentY);
         });
         $conver.mousedown(function(e_){
        	 if(e_.which == 1){
        		 console.info("test");
	        	 moveFlag=true;
	             clickFlag=true;
        	 }
         });
         
         
         
         $("#cover_content").mousemove(function(e_){
                clickFlag=false;
                var me =window.event || e_;
                if(moveFlag && !clickFlag){
                    //将鼠标坐标传给Canvas中的图像
                    //下面四个条件为限制div以及图像的活动边界
                    if(me.clientX-25 < wholeX){
	                   	 $conver.css("left",minWidth +"px");
                    }else if(me.clientY-25  < wholeY){
	                   	 $conver.css("top", minHeight +"px");
                    }else if(me.clientX+25 > wholeX+canvasW){
	                   	 $conver.css("left",maxWidth+"px");
                    }else if(me.clientY+25 > wholeY+canvasH){
	                   	 $conver.css("top",maxHeight +"px");
                    }else{
	                    $conver.css("left",me.clientX - wholeX - 25 +"px");
		                $conver.css("top",me.clientY - wholeY - 25 +"px");
                    }
                }
            });
  
  
 */ 
