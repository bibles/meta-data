/**
 * Created by Administrator on 2017-11-11.
 */
/*
 多图删除
 */
function update_del(obj) {
    $(obj).parent().parent().remove();
}
/*
 预览
 */
function open_url(obj) {
    var item = $(obj).parent().parent(".item_img");
    window.open(item[0].id);
}
/*
 多图上传变换左右位置
 */
function toleft(obj){
    var item = $(obj).parent().parent(".item_img");
    var item_left = item.prev(".item_img");
    if ($(obj).parent().parent().parent().children(".item_img").length >= 2) {
        if (item_left.length == 0) {
            item.insertAfter($(obj).parent().parent().parent().children(".item_img:last"))
        } else {
            item.insertBefore(item_left)
        }
    }
}

function toright(obj){
    var item = $(obj).parent().parent(".item_img");
    var item_right = item.next(".item_img");
    if ($(obj).parent().parent().parent().children(".item_img").length >= 2) {
        if (item_right.length == 0) {
            item.insertBefore($(obj).parent().parent().parent().children(".item_img:first"))
        } else {
            item.insertAfter(item_right)
        }
    }
};