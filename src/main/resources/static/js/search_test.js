


// 节流
//**********************************************
var _timer = {};
//输入间隔wait(默认0.5)秒内 视作连续输入 不做查询
function delay_till_last(id, fn, wait) {
    if (_timer[id]) {
        window.clearTimeout(_timer[id]);
        delete _timer[id];
    }

    return _timer[id] = window.setTimeout(function () {
        fn();
        delete _timer[id];
    }, wait);
}
//键盘离开事件   打字停顿事件
/*$("#contract_no1").on('keyup', function (e) {
    //监听键盘方向键↑↓
    if (e.keyCode == 38 || e.keyCode == 40) {
        //获取 搜索返回结果集合
        var $searchLi = $("#J_searchData ul li");
        if ($searchLi.length > 0) {
            //获取选中的行的索引
            var $index = $("#J_searchData ul").find("li.active").index();
            //被选中的情况下
            if ($index > -1) {
                //对选择后的 下标 进行求余
                $index=($index+(e.keyCode == 40 ? 1 : -1))%$searchLi.length;
                $searchLi.removeClass("active");
                $searchLi.eq($index).addClass("active")
            }else if($index==-1){
                //一个都没被选中的情况下
                if (e.keyCode == 40) {
                    $searchLi.first().addClass("active")
                } else {
                    $searchLi.last().addClass("active")
                }
            }
        }
        return
    }
    //选中后 监听回车键 Enter
    if (e.keyCode == 13) {
        //赋值
        $("#contract_no1").val($("#J_searchData ul").find("li.active").html())
        $("#J_searchData ul").hide();
        //去查询
        hetong()
        //停止整个搜索查询
        return false;
    }


    delay_till_last('myclick', function () {
        // run something

        $.ajax({
            url: "/layui/table/list",
            data: {
                querydata: JSON.stringify({contract_no: $("#contract_no1").val()}),
                resource_name: "bgzdh_project"
            },
            async: false,
            success: function (data) {
                console.log(data)
                if (data.success) {
                    if (data.data.length > 0) {
                        //显示查询结果
                        $(".J_searchData_select").show();
                    }
                    //清空上一次查询结果
                    $("#J_searchData ul").html("")
                    $.each(data.data, function (i, val) {
                        $("#J_searchData ul").append("<li class='mouse-hover' >" + val.contract_no + "</li>");
                    })
                    //鼠标 选中结果后 触发
                    $("#J_searchData ul li").click(function () {
                        $("#contract_no1").val($(this).html())
                        $("#J_searchData ul").hide();
                        /!*alert("成功")*!/
                        hetong()
                    })
                }
            }
        })
    }, 500);
});*/


//实现搜索框功能必须调用此方法
function search_test(id,url,data123) {
    // console.log(data123)
    //注册当前查询框
    var _this=$("#"+id);
    var _div=$("<div id='searchData_"+id+"'>");
    var _ul=$("<ul class='searchData_select'>")
    _div.append(_ul);
    _this.parent().append(_div);

    //监听键盘事件
    _this.on('keyup', function (e) {
        //监听键盘方向键↑↓
        if (e.keyCode == 38 || e.keyCode == 40) {
            //获取 搜索返回结果集合
            var _li = _ul.children("li");
            if (_li.length > 0) {
                //获取选中的行的索引
                //var $index = $("#J_searchData ul").find("li.active").index();
                var $index = _ul.find("li.active").index();
                //被选中的情况下
                if ($index > -1) {
                    //对选择后的 下标 进行求余
                    $index=($index+(e.keyCode == 40 ? 1 : -1))%_li.length;
                    _li.removeClass("active");
                    _li.eq($index).addClass("active")
                }else if($index==-1){
                    //一个都没被选中的情况下
                    if (e.keyCode == 40) {
                        _li.first().addClass("active")
                    } else {
                        _li.last().addClass("active")
                    }
                }
            }
            return
        }
        //选中后 监听回车键 Enter
        if (e.keyCode == 13) {
            //赋值
            _this.val(_ul.find("li.active").html())
            _ul.hide();
            //去查询
            hetong()
            //停止整个搜索查询
            return false;
        }
        //console.log(this.value);


        delay_till_last('myclick', function () {
            //console.log($("#parent_id").val());
            // run something
            var str='{'+id+':"'+$("input[name="+id+"]").val()+'"}'
            var querydata=eval("("+str+")");
            //var querydata="{'"+id+"':'"+$("input[name="+id+"]").val()+"'}";
            data123={

                querydata: JSON.stringify(querydata),
                resource_name: "bgzdh_project"
            }
            $.ajax({
                url: url,
                data: data123,
                async: false,
                success: function (data) {
                    // console.log(data)
                    if (data.success) {
                        if (data.data.length > 0) {
                            //显示查询结果
                            _ul.show();
                        }
                        //清空上一次查询结果
                        _ul.html("")
                        $.each(data.data, function (i, val) {
                            _ul.append("<li class='mouse-hover' >" + val.contract_no + "</li>");
                        })
                        //鼠标 选中结果后 触发
                        _ul.children("li").click(function () {
                            _this.val($(this).html())
                            _ul.hide();
                            hetong()
                        })
                    }
                }
            })
        }, 500);
    });
}
