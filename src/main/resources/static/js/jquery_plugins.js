/**
 * Created by xiaoymin on 2016/7/28.
 */
(function ($) {
    //jquery global全局事件
    $.ajaxSetup({
        error:function (xhr, status, e) {
            //用户未登录
            if(xhr.status==302){
                window.location="/login.html";
            }
        }
    })
})(jQuery)
